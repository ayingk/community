# Review of Usability SIG Tech Session on May 28th

![usability](./images/usability.png)

On May 28th, 2022, MindSpore Usability SIG held its second online technology sharing event, with the theme of "Start from Scratch, Start with Easy". A total of 265 developers participated in the event through Tencent conference and B station live broadcast.

This event brought 87 new developers to the Usability SIG Exchange Group. Thank you for your enthusiastic participation and wonderful sharing. Here we have compiled the summary of the activity as well as the questions and answers. Hope you are satisfied!

## Review of Lectures

The theme of this activity is 'Starting from scratch, Easy to Get started', 5 speakers mainly focused on the main problems that MindSpore beginners may encounter when learning and starting MindSpore, from the perspectives of installation, programming assistance, debugging and tuning, model migration and so on.

Thanks to Zhang Hui, Chissica, Miao, Louie and CQU dizhongdi for your wonderful sharing!

**1. MindSpore Roaming the World**

Senior MindSpore developer Zhang Hui (known as Teacher Zhang Xiaobai in the community) opened a new perspective through vivid and interesting explanations, introducing the strange scenery (functional features) of various parallel worlds (components) of MindSpore as well as the installation method of MindSpore on multiple platforms.

![lecturer1](./images/lecturer1.jpg)

**2. Intelligent Assisted Programming**

Chissica, technical director of MindSpore Dev Toolkit and MindSpore sermon, based on his expertise in the fields of code recommendation, code search and annotation generation, introduced the key technology in intelligent software development -- the core technology and application scenarios of intelligent aided programming. These technologies have also been implemented in the MindSpore Dev Toolkit plug-in, and you are welcome to experience them.

![lecturer2](./images/lecturer2.jpg)

**3. Thoughts on AI Framework Function Debugging and MindSpore Practice**

Miao and Louie, two MindSpore DFx experts, introduced their systematic thinking in the field of AI framework function debugging, and shared common problems and solutions in machine learning development function debugging based on MindSpore.

![lecturer3](./images/lecturer3.jpg)
![lecturer4](./images/lecturer4.jpg)

**4. MindSpore Model Migration Experience Sharing**

Cqudi Zhongdi, a MindSpore ease-of-use expert, takes BERT as an example to share the methods and experience related to model migration from PyTorch/Tensorflow to MindSpore.

![lecturer5](./images/lecturer5.jpg)

## Q&A

**Question (JeffDing) : Is an alchemist just able to train code? **

Answer (Zhang Hui) : AI development involves a lot of knowledge and skills. Everyone should find their own way to learn and use AI framework according to their own strengths. For example, if I personally learn framework from installation, maybe someone else would be more appropriate to start with model tuning, AI for Science, etc. After learning some basic knowledge and skills, you can take a step closer.

**Question: Where can I download the MindSpore Dev Toolkit for trial? **

Answer: (Chissica) MindSpore code warehouse with a child in the warehouse: https://gitee.com/mindspore/ide-plugin, you can download the trial here.

**Question (audience at STATION B) : In the future, will a programmer say a requirement and the AI implement it directly? **

Answer (Chissica) : This is a very big and ambitious goal, but only part of it can be achieved at present. For example, AlphaCode made by DeepMind in the industry can solve part of the algorithm problems, and give the description of the algorithm problem to give the executable code. Ai-assisted programming is currently only able to achieve certain scenarios in a single field, but not so ideal. Personally, in the future, it may not be completely ideal, because in the process of software development, there are some creative activities, such as demand decomposition, which cannot be done by artificial intelligence at present. At least it will take 5-10 years to develop, but it is still a technology worth exploring.

**Question (Tong) : How to evaluate the merits/demerits/maturity of an intelligent auxiliary programming system? **

Answer (Chissica) : On the one hand, it can be measured by reducing the number of keyboard strokes by programmers. The less keyboard strokes by programmers, the more complete the auxiliary programming ability of AI is. On the other hand, the fewer changes programmers make, the more accurate and effective the recommendations are. By extension, in the future, if there are strong AI-aided programming tools that can directly recommend executable programs, it can be said that they have reached a peak state.

## Related Materials

1. The activity of speech materials will be released to the MindSpore community, please click on the [link](https://gitee.com/mindspore/community/tree/master/sigs/usability).
2. The video of this activity will be uploaded to the official account of MindSpore at site B, please pay attention.

---
Join the SIG communication group to build an easy-to-learn, flexible and efficient AI framework together!

Way to join: add the wechat of the assistant (wechat: mindspore0328). Please note 'Usability SIG' when you are adding.

Assistant will invite you into the group!